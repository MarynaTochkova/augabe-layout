import Vue from 'vue'
import Router from 'vue-router'
import AllWidgets from './components/AllWidgets'
import Offer from './components/Offers'
import Incomes from './components/Incomes'
import Outcomes from './components/Outcomes'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/offers',
            name: 'offers',
            component: Offer
        },
        {
            path: '/incomes',
            name: 'incomes',
            component: Incomes
        },
        {
            path: '/outcomes',
            name: 'outcomes',
            component: Outcomes
        },
        {
            path: '/',
            name: 'all',
            component: AllWidgets
        }
    ]
})