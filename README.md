**Aufgabe-Layout**
Responsive Layout for FastBill

Tested in:

 - Chrome
 - Firefox

Used technologies:

 - VueJS
 - Vuetify
 - JavaScript
 - CSS

Implemented functionality:

 - [x] Base item list component with ability to add new items and show all items
 - [x] Widgest for each category
 - [x] Widget with all categories
 - [x] Responsive menu 
 - [x] Footer 

Version 1.0

to do:

 - [ ] Search functionality
 
 

> Created by Maryna Tochkova.  All rights reserved

